<?php

/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*
* The module file calls the function user_storefront_html in this file to
* process the Free Simple Shop functions and produce the page content.
* Originally this was done because this file would determine which theme was
* active and would include the theme php file for the active theme. That has
* been changed and only theme_default.php is now used.
*/

function user_storefront_html($storeowner_uid, $access_as_admin) {
  global $user;

  $content = '';

  if ($access_as_admin) {
    // permission to access as admin should have already been checked
    $baseurl = 'admin';

    //if session vars for admin not already set, set them
    if (!$_SESSION['userstorefront'][$storeowner_uid]['admin_id']) {

      //permission to set admin mode should already have been verified
      $_SESSION['userstorefront'][$storeowner_uid]['admin_id'] = $user->uid; //$db_admin_id;
      $_SESSION['userstorefront'][$storeowner_uid]['admin_first_name'] = $user->name; //this is username, should be real name / $db_admin_first_name;
      $_SESSION['userstorefront'][$storeowner_uid]['admin_last_name'] = ''; //should be real last name / $db_admin_last_name;
      $_SESSION['userstorefront'][$storeowner_uid]['admin_email_address'] = $user->mail; //$db_admin_email_address;
      $_SESSION['userstorefront'][$storeowner_uid]['admin_password'] = ''; //$db_admin_password;
      $_SESSION['userstorefront'][$storeowner_uid]['admin_rights'] = 'Admin'; //$db_admin_rights;

      $_SESSION['userstorefront'][$storeowner_uid]['login'] =  'admin';


    }
  }
  else {
    $baseurl = 'storefront';

    //check if logged in as admin, if so clear session data
    if ($_SESSION['userstorefront'][$storeowner_uid]['admin_id']) {
      $_SESSION['userstorefront'][$storeowner_uid] = array();
    }

    //If accessing certain pages (checkout, orders, service) then:
    //if logged into drupal, login to store as member
    //check if drupal member id in usf member table
    if (($user->uid > 0) and
    (
        ($_GET['page'] == 'checkout_phase_two') or
        ($_GET['page'] == 'checkout_phase_three') or
        ($_GET['page'] == 'checkout_phase_four') or
        ($_GET['page'] == 'checkout_phase_five') or
        ($_GET['page'] == 'orders') or
        ($_GET['page'] == 'member_orders') or
        ($_GET['page'] == 'service')
    )) {



      $query = "SELECT id, uid, first_name, last_name FROM {usf_members} WHERE uid = " . $user->uid;

      $db_lookup_member = db_query($query);
      $db_member = @db_fetch_array($db_lookup_member);

      $_SESSION['userstorefront']['member_id'] = $db_member['id'];
      $_SESSION['userstorefront']['member_first_name'] = $db_member['first_name'];
      $_SESSION['userstorefront']['member_last_name'] = $db_member['last_name'];
      $_SESSION['userstorefront']['member_email_address'] = $user->mail;
      $_SESSION['userstorefront']['member_password'] = "na";

      $_SESSION['userstorefront'][$storeowner_uid]['login'] = "member";
    }
    else {
      //if not logged into drupal and viewing one of the pages in the if condition above, then set vars so that member not logged in
      unset($_SESSION['userstorefront']['member_id']);
      unset($_SESSION['userstorefront']['member_first_name']);
      unset($_SESSION['userstorefront']['member_last_name']);
      unset($_SESSION['userstorefront']['member_email_address']);
      unset($_SESSION['userstorefront']['member_password']);

      unset($_SESSION['userstorefront'][$storeowner_uid]['login']);

    }
  }

  //add store uid to base url

  $baseurl .= '?uid=' . $storeowner_uid;

  //set up some basic things that originally were in themes.inc
  include('includes/settings.php');

  $db_themes_website_name = user_storefront_get_storesetting($storeowner_uid, 'store_title');

  drupal_set_title($db_themes_website_name);

  $db_themes_website_name = str_replace("\\", "", $db_themes_website_name);

  $page = $_GET['page'];
  $request = $_GET['request'];

  $admin_id = $_SESSION['userstorefront']['admin_id'];
  $members_id = $_SESSION['userstorefront']['member_id'];

  $header = "themes_header.inc";
  $body = "themes_body.inc";
  $footer = "themes_footer.inc";

  ob_start();

  include('includes/default_theme.php');

  $content .= ob_get_contents();
  ob_end_clean();

  return $content;

}



?>
