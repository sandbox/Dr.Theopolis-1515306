User Storefront

Description
-----------
The User Storefront module is a shopping cart module  which allows users to set up and operate online stores where each store operates mostly independently. Each user store has it's own storefront page, and it's own settings for payments. It works like Etsy in that customers have a separate shopping cart for each store, and if they have items in more than one shopping cart, they checkout of each one separately, paying each merchant directly.
 
The User Storefront module is developed by the St. Augustine Artist Co-op (www.saaco.org). One of SAACO's goals is to operate an online marketplace, somewhat similar to Etsy except more on a local level. There was a need for shopping cart software to accomplish this, and we found no free solutions that work like Etsy in that there are separate shopping carts for each merchant.

Requirements
------------
This module may not work correctly if you have the jQuery Update module installed. There is a problem with AHAH forms that include file fields causing the AHAH form to not work properly. If you have problem with dropdown boxes not working properly, you can either uninstall the jQuery Update module, look for an updated version of it, or if you must have it, see http://drupal.org/node/806500 for informatino about patching it.

Installation
------------
Install as usual. Enable the User Storefront blocks in order for the menus to show as designed to both storefront admins and customers.


Credits
-------
This module is based on the Free Simple Shop software Developed by Dustin Cowell Enterprises. The conversion to Drupal was initially done by Matt Thomas representing the St. Augustine Artist Co-op (www.saaco.org/user-storefront-module).

 