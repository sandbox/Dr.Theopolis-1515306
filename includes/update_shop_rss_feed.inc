<?php
/**
* @file
* Updates an rss feed file for the user shop.
*
* This code has not been rewritten yet for the multiuser version.
* The include line in products.inc has been commented out.
*/

    $db_themes_website_name = user_storefront_get_storesetting($storeowner_uid, 'store_title');

    $file = "shop_feed.xml";
    $method = fopen($file, 'w');
    $data = "<?xml version=\"1.0\"?>\r";
    fwrite($method, $data);
    $data = "<rss version=\"2.0\">\r";
    fwrite($method, $data);
    $data = "<channel>\r";
    fwrite($method, $data);
    $db_themes_website_name = str_replace("\\", "", $db_themes_website_name);
    $db_themes_website_name = htmlspecialchars("$db_themes_website_name", ENT_QUOTES);
    $data = "\r<title>$db_themes_website_name Shop Feed</title>\r";
    fwrite($method, $data);
    $data = "<link>http://" . $_SERVER['HTTP_HOST'] . "" . $baseurl . "</link>\r";
    fwrite($method, $data);
    $data = "<description>Latest Products</description>\r";
    fwrite($method, $data);
    $data = "<lastBuildDate>$rss_date</lastBuildDate>\r";
    fwrite($method, $data);
    $data = "<language>en-us</language>\r\r";
    fwrite($method, $data);

    $db_lookup_products = db_query("SELECT id, rss_date, code, name, price, width, depth, height, short_description, long_description, category_id FROM {usf_products} WHERE uid = $storeowner_uid AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 0, 20");

    while ($db_products = @db_fetch_array($db_lookup_products)) {

      $db_products_id = $db_products['id'];
      $db_products_rss_date = $db_products['rss_date'];
      $db_products_code = $db_products['code'];
      $db_products_name = $db_products['name'];
      $db_products_price = $db_products['price'];
      $db_products_width = $db_products['width'];
      $db_products_depth = $db_products['depth'];
      $db_products_height = $db_products['height'];
      $db_products_short_description = $db_products['short_description'];
      $db_products_long_description = $db_products['long_description'];
      $db_products_category_id = $db_products['category_id'];

      $db_products_short_description = str_replace("\r", '<br>', $db_products_short_description);
      $db_products_short_description = str_replace("\n", "<p>", $db_products_short_description);
      $db_products_short_description = str_replace("<br><p><br><p>", "<p>", $db_products_short_description);
      $db_products_short_description = str_replace("<br><p>", "<br>", $db_products_short_description);
      $db_products_short_description = str_replace("\\", "", $db_products_short_description);

      $db_products_long_description = str_replace("\r", '<br>', $db_products_long_description);
      $db_products_long_description = str_replace("\n", "<p>", $db_products_long_description);
      $db_products_long_description = str_replace("<br><p><br><p>", "<p>", $db_products_long_description);
      $db_products_long_description = str_replace("<br><p>", "<br>", $db_products_long_description);
      $db_products_long_description = str_replace("\\", "", $db_products_long_description);

      $db_lookup_product_categories = db_query("SELECT name FROM {usf_products_categories} WHERE uid = $storeowner_uid AND id='$db_products_category_id'");
      $db_product_categories = @db_fetch_array($db_lookup_product_categories);
      $db_product_categories_name = $db_product_categories['name'];

      $data = "<item>\r";
      fwrite($method, $data);
      $db_products_name = str_replace("\\", "", $db_products_name);
      $db_products_code = str_replace("\\", "", $db_products_code);
      $db_products_name = htmlspecialchars("$db_products_name", ENT_QUOTES);
      $db_products_code = htmlspecialchars("$db_products_code", ENT_QUOTES);
      if ($name) {
        $data = "<title>$db_products_name</title>\r";
      } else {
        $data = "<title>$db_products_code</title>\r";
      }
      fwrite($method, $data);
      $data = "<link>http://" . $_SERVER['HTTP_HOST'] . "" . $baseurl . "&page=view_detail&amp;request=$db_products_id</link>\r";
      fwrite($method, $data);
      $data = "<guid>http://" . $_SERVER['HTTP_HOST'] . "" . $baseurl . "&page=view_detail&amp;request=$db_products_id</guid>\r";
      fwrite($method, $data);
      $data = "<description><![CDATA[";
      fwrite($method, $data);

      $db_lookup_products_images = db_query("SELECT small_image_name FROM {usf_products_images} WHERE uid = $storeowner_uid AND products_id='$db_products_id' and main_image='yes'");
      $db_products_images = @db_fetch_array($db_lookup_products_images);
      $db_products_images_small_image_name = $db_products_images['small_image_name'];

      if ($db_products_images_small_image_name) {
        if ($db_products_long_description) {
          $data = "<img src='" . $image_view_dir . "shop_images/$db_products_images_small_image_name' border='0'><p> $db_products_long_description";
        } else {
          $data = "<img src='" . $image_view_dir . "shop_images/$db_products_images_small_image_name' border='0'><p> $db_products_short_description";
        }
      } else {
        if ($db_products_long_description) {
          $data = "$db_products_long_description";
        } else {
          $data = "$db_products_short_description";
        }
      }
      if ($db_products_width or $db_products_depth or $db_products_height) {
        $data .= "<br>";
      }
      if ($db_products_height) {
        $data .= "Height: $db_products_height ";
      }
      if ($db_products_width) {
        $data .= "Width: $db_products_width ";
      }
      if ($db_products_depth) {
        $data .= "Depth: $db_products_depth ";
      }
      if ($db_products_price) {
        $data .= "<br>Price: \$$db_products_price";
      }
      fwrite($method, $data);
      $data = "]]></description>\r";
      fwrite($method, $data);
      if ($db_product_categories_name) {
        $data = "<category>$db_product_categories_name</category>\r";
        fwrite($method, $data);
      }
      $data = "<pubDate>$db_products_rss_date</pubDate>\r";
      fwrite($method, $data);
      $data = "</item>\r\r";
      fwrite($method, $data);
    }
    $data = "</channel>\r";
    fwrite($method, $data);
    $data = "</rss>";
    fwrite($method, $data);
    fclose($method);
?>