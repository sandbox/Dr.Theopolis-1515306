<?php
/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*
* Contains code that deals with viewing orders for customers
*/

  echo("Welcome " . $members_first_name . " " . $members_last_name . "!");

  if (!isset($_GET['uid'])) {
    drupal_set_title('All Orders');
    $where_uid_and = '{%d}';
    $include_store_id = true;
  }
  else {
    $where_uid_and = "uid = %d AND ";
    $include_store_id = false; //already in baseurl
    echo "You are viewing orders from this store. <a href='/storefront?page=orders'>Click here to view orders from all stores</a>.<br><br>";
  }

  $db_lookup_orders = db_query("SELECT id FROM {usf_orders} WHERE $where_uid_and member_id='%s'", $storeowner_uid, $members_id);
  $db_orders = @db_fetch_array($db_lookup_orders);
  $db_orders_id = $db_orders['id'];

  if ($db_orders_id) {

    echo("<hr size='1' color='#000000'><p>");

    echo("<b>Latest Orders</b><p>");

    echo("<table border='0' cellpadding='5' cellspacing='0'>");
    echo("<tr>");
    echo("<td>" . "<b>ID</b></td>");
    echo("<td>" . "<b>Date</b></td>");
    echo("<td>" . "<b>Time</b></td>");
    echo("<td>" . "<b>Payment</b></td>");
    echo("<td>" . "<b>Total</b></td>");
    echo("<td>" . "<b>Actions</b></td>");
    echo("</tr>");

    $db_lookup_orders = db_query("SELECT id, uid, date, time, payment_status, order_total FROM {usf_orders} WHERE $where_uid_and member_id='%s' LIMIT 0, 5", $storeowner_uid, $members_id);

    while ($db_orders = @db_fetch_array($db_lookup_orders)) {

      $db_orders_id = $db_orders['id'];
      $db_orders_date = $db_orders['date'];
      $db_orders_time = $db_orders['time'];
      $db_orders_member_id = $db_orders['member_id'];
      $db_orders_payment_status = $db_orders['payment_status'];
      $db_orders_order_total = $db_orders['order_total'];
      if ($include_store_id) {
        $db_store_id = "?uid=" . $db_orders['uid'];
      }

      echo("<tr><td>" . $db_orders_id . "</td>");
      echo("<td>" . $db_orders_date . "</td>");
      echo("<td>" . $db_orders_time . "</td>");
      echo("<td>" . $db_orders_payment_status . "</td>");
      echo("<td>" . "$" . number_format($db_orders_order_total,2) . "</td>");
      echo("<td>" . "[<a href='" . $baseurl . "$db_store_id&page=member_orders&request=view_order&order_id=$db_orders_id'>" . $link_color . "View</a>]</td>");
      echo("</tr>");
    }
    echo("</table>");
  }

  $db_lookup_service = db_query("SELECT id FROM {usf_service} WHERE $where_uid_and member_id='%s'", $storeowner_uid, $members_id);
  $db_service = @db_fetch_array($db_lookup_service);
  $db_service_id = $db_service['id'];

  if ($db_service_id) {

    echo("<hr size='1' color='#000000'><p>");

    echo("<b>Latest Service Requests</b><p>");

    echo("<table border='0' cellpadding='5' cellspacing='0'>");
    echo("<tr>");
    echo("<td>" . "<b>Order ID</b></td>");
    echo("<td>" . "<b>Status</b></td>");
    echo("<td>" . "<b>Actions</b></td>");
    echo("</tr>");

    $db_lookup_service = db_query("SELECT id, uid, member_id, order_id, status FROM {usf_service} WHERE $where_uid_and member_id='%s'", $storeowner_uid, $members_id);

    while ($db_service = @db_fetch_array($db_lookup_service)) {

      $db_service_id = $db_service['id'];
      $db_service_member_id = $db_service['member_id'];
      $db_service_order_id = $db_service['order_id'];
      $db_service_status = $db_service['status'];
      if ($include_store_id) {
        $db_store_id = "?uid=" . $db_service['uid'];
      }

      $db_lookup_members = db_query("SELECT first_name, last_name FROM {usf_members} WHERE $where_uid_and id='%s'", $storeowner_uid, $db_service_member_id);

      while ($db_members = @db_fetch_array($db_lookup_members)) {

        $db_members_first_name = $db_members['first_name'];
        $db_members_last_name = $db_members['last_name'];

          echo("<tr>");
          echo("<td>" . $db_service_order_id . "</td>");
          echo("<td>" . $db_service_status . "</td>");
          echo("<td>" . "[<a href='" . $baseurl . "$db_store_id&page=member_service&request=view_service&service_id=$db_service_id'>" . $link_color . "View</a>]");
          echo("</td>");
          echo("</tr>");
      }
    }
    echo("</table>");
  }

?>