<?php
/**
* @file
* User Storefront global setting and global admin functions
*/

/**
 *
 */
function user_storefront_get_globalsetting($setting_type) {
  $query = "SELECT setting FROM {usf_global_settings} WHERE setting_type = '%s'";
  $db_result = db_query($query, $setting_type);
  $db_data = db_fetch_array($db_result);
  return stripslashes($db_data['setting']);
}

/**
 *
 */
function user_storefront_set_globalsetting($setting_type, $setting_value) {
  $setting_value = addslashes($setting_value);
  $query = "UPDATE {usf_global_settings} SET setting = '%s' WHERE setting_type = '%s'";
  db_query($query, $setting_value, $setting_type);
}

/**
 *
 */
function user_storefront_globalsettings() {
  return drupal_get_form('user_storefront_globalsettings_form');
}

/**
 *
 */
function user_storefront_globalsettings_form() {

  $form['paypalsandbox'] = array(
    '#type' => 'radios',
    '#title' => t('Use Paypal Sandbox Server'),
    '#description' => t('Choose if you would like to use Paypal sandbox server for testing. Disable if you want to use production server.'),
    '#default_value' => user_storefront_get_globalsetting('paypal_sandbox'),
    '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );
  return $form;

}

/**
 *
 */
function user_storefront_globalsettings_form_submit($form_id, $form_values) {
  user_storefront_set_globalsetting('paypal_sandbox', $form_values['values']['paypalsandbox']);

  drupal_set_message(t('Your settings has been saved.'));
}

/**
 * Import product categories from pasted text
 */
function user_storefront_importcategories() {
  return drupal_get_form('user_storefront_importcategories_form');
}

/**
 * Import product categories form
 */
function user_storefront_importcategories_form() {
  $form['#attributes'] = array(
    'enctype' => "multipart/form-data",
  );

  $form['pasted_input'] = array(
    '#type' => 'textarea',
    '#title' => t('Past CSV formatted text: category-id, parent-id, "category-name".'),
    '#default_value' => "",
    '#rows' => 10,
    '#wysiwyg' => FALSE,
    '#required' => TRUE,
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => '</div>',
  );

  return $form;
}


/**
 * Separate CSV values into array
 */
function user_storefront_csv_explode($str, $delim = ',', $qual = "\"")
{
  if ((PHP_MAJOR_VERSION > 5) or ((PHP_MAJOR_VERSION == 5) and (PHP_MINOR_VERSION >= 3))) {
    return str_getcsv($str);
  }
  else {

   $skipchars = array( $qual, "\\" );
   $len = strlen($str);
   $inside = false;
   $word = '';
   for ($i = 0; $i < $len; ++$i) {
       if ($str[$i]==$delim && !$inside) {
           $out[] = $word;
           $word = '';
       } else if ($inside && in_array($str[$i], $skipchars) && ($i<$len && $str[$i+1]==$qual)) {
           $word .= $qual;
           ++$i;
       } else if ($str[$i] == $qual) {
           $inside = !$inside;
       } else {
           $word .= $str[$i];
       }
   }
   $out[] = $word;
   return $out;
  }
}


/**
 * Import product categories form submit
 */
function user_storefront_importcategories_form_submit($form, &$form_state) {

  $lines = explode("\n", $form_state['values']['pasted_input']);
  foreach($lines as $line) {
    $row = user_storefront_csv_explode(trim($line));

    if (count($row) < 3) {
      continue;
    }

    db_query("INSERT INTO {usf_site_categories} SET category_id = $row[0], parent_category_id = $row[1], name = '$row[2]';");
  }

  drupal_set_message(t('Done importing categories'));
}

?>