<?php
/**
* @file
* User Storefront API functions for other modules to call.
*/

/**
 * Get a list of main store categories
 * @return hierarchical list of store category ids and names
 */
function user_storefront_api_storecategories() {


}

/**
 * See if store exists for given store id
 * @param store id
 * @return true or false
 */
function user_storefront_api_storeexists($storeid) {
   return user_storefront_storeexists($storeid);
 }

/**
 * Get list of all stores
 * @param exclude stores that have no active non zero items
 * @param include only stores with this category
 * @return array of store id's
 */
function user_storefront_api_storelist($exclude_zero_inventory=FALSE, $store_category_id=NULL) {
  $list = array();

  //store id
}

/**
 * Get store information
 * @param store id
 * @return array of details
 */
function user_storefront_api_storeinfo($storeid) {
  //store name
  //store owner name maybe
  //url to storefront (base url)
  //url to store square header graphic
  //url to store banner graphic
}

/**
 * Get product ids (and therefore count) for a store
 * @param storeid
 * @return array of active product ids
 */
function user_storefront_api_productids($storeid, $include_out_of_stock=FALSE) {

}

/**
 * Get $count number of product ids for latest products added w non zero inventory, sitewide or for given store
 * @param max count to retrieve
 * @param optional store id or if null, all stores
 * @return array of active product ids
 */
function user_storefront_api_latestproducts($count, $storeid=NULL) {

}

/**
 * Get array of details about a given product id
 * @param product id
 * @return array of details or FALSE if product does not exist
 */
function user_storefront_api_product($productid) {
  $db_lookup_product = db_query("SELECT * FROM {usf_products} WHERE id =%d;", $productid);
  $db_product = @db_fetch_array($db_lookup_product);
  if ($db_product) {
    return $db_product;
  }
  else {
    return FALSE;
  }
}

/**
 * Get array of image details for each images for a given product id
 * @param product id
 * @param store id which technically isn't needed because product ID's are supposed to be unique sitewide
 * @return array of details of each product image or
 *   returns FALSE if product ID was not found or
 *   return empty array if there are no images.
 *   Returned array has sequently numeric keys which do not correspond to the product ID.
 *   If there are any images, element [0] is the main image.
 *   Each array element is an array as such:
 *     ['product_image_id'] = product image id (not the product id)
 *     ['small_image_url'] = url of small image
 *     ['large_image_url'] = url of large image
 *     ['main_image'] = TRUE or FALSE
 */
function user_storefront_api_product_images($productid, $storeid=NULL) {
  if (user_storefront_api_product($productid) == FALSE) {
    return FALSE;
  }

  $images = array();

  //store id isn't really necessary but in case you want to make absolute sure the product id
  //is associated with a given store id, you can provide the store id
  if (isset($storeid)) {
    $db_images_lookup = db_query("SELECT * FROM {usf_products_images} WHERE uid = %d AND products_id = %d ORDER BY id;", $storeid, $productid);
  }
  else {
    $db_images_lookup = db_query("SELECT * FROM {usf_products_images} WHERE products_id = %d ORDER BY id;", $productid);
  }

  $item_number = 1;

  while ($db_image = db_fetch_array($db_images_lookup)) {
    if (!$image_view_dir) {
      $image_view_dir = user_storefront_imageviewdir($db_image['uid']);
    }

    if ($db_image['main_image'] == 'yes') {
      $put_in_item = 0;
    }
    else {
      $put_in_item = $item_number;
      $item_number++;
    }

    $images[$put_in_item]['product_image_id'] = $db_image['id'];
    $images[$put_in_item]['small_image_url'] = $image_view_dir . $db_image['small_image_name'];
    $images[$put_in_item]['large_image_url'] = $image_view_dir . $db_image['large_image_name'];
    $images[$put_in_item]['main_image'] = ($db_image['main_image'] == 'yes');
    $images[$put_in_item]['small_image_name'] = $db_image['small_image_name'];
    $images[$put_in_item]['large_image_name'] = $db_image['small_image_name'];
  }

  //if main image was not the first in the db query result, it will not be first item in array
  //so ksort to put it in first item
  ksort($images);

  return $images;
}

/**
 * Get array of order ids for a customer
 * @param drupal uid of customer
 * @return array of order ids
 */
function user_storefront_api_orderids($customer_uid) {

}

/**
 * Get array of order details
 * @param order id
 * @return array of details about the order
 */
function user_storefront_api_order($customer_uid) {

}




?>