<?php
/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*
* This used to be an elaborate html template which is now barely needed.
*/
?>
  <div class="postcontent">
  <table border="0" cellpadding="10" cellspacing="0" width="100%">
    <tr>
	    <td valign="top" align="left" width="100%">
	    <?php include($body); ?>
	    </td>
    </tr>
  </table>
  </div>

