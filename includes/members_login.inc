<?php
/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*
* This file sets up variables used by the rest of the code to indicate that the
* user logged in is a customer with a member account. This status is mainly
* used for checking out and viewing order history. The Free Simple Shop code
* was written so that a member could not be logged in to view products in the
* store, so in the Drupal version even if the customer is logged into their
* Drupal account, they are not "logged" into the storefront at all times - only
* for checking out or viewing order history.
*/

$members_id = $_SESSION['userstorefront']['member_id'];
$members_first_name = $_SESSION['userstorefront']['member_first_name'];
$members_last_name = $_SESSION['userstorefront']['member_last_name'];
$members_email_address = $_SESSION['userstorefront']['member_email_address'];
$members_password = $_SESSION['userstorefront']['member_password'];


$page = $_GET['page'];
$request = $_GET['request'];

$results_per_page = $_SESSION['userstorefront'][$storeowner_uid]['results_per_page'];

?>