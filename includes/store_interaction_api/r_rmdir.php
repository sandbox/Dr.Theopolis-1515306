<?php

function r_rmdir($dir, $delete_dir_too = FALSE) {
  foreach (glob($dir . '/*') as $file) {
    if (is_dir($file)) {
      r_rmdir($file);
    }
    else {
      unlink($file);
    }
  }
  if ($delete_dir_too) {
    rmdir($dir);
  }
}

?>