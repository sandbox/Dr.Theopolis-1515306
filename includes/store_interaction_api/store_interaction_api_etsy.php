<?php
/**
* @file
* Store Interaction API functions for etsy.com.
* The term "Etsy" is a trademark of Etsy, Inc.
* This API is not endorsed or certified by Etsy, Inc.
* This API does not use the Etsy API.
*/

require_once("parser/simple_html_dom.php");
require_once("store_interaction_api_etsy_internal.php");

/**
* Get array of top level categories
*/
function store_interaction_api_etsy_gettopcategories() {
  return store_interaction_internal_etsy_getcategories("http://www.etsy.com", "ul", "category-list");

}

/**
* Get array of subcategories from given page url
*/
function store_interaction_api_etsy_getsubcategories($url) {
  return store_interaction_internal_etsy_getcategories($url, "ul", "category-nav");
}

/**
* Get array of sub sub categories from given page url
*/
function store_interaction_api_etsy_getsubsubcategories($url) {
  $categories = array();

  $page = file_get_html($url);

  if ($page) {
    $ul = $page->find("ul[id=category-nav]",0);
    if ($ul) {
      $li = $ul->find("li[class=active]", 0);


      $subsubcatlist = $li->find("ul", 0);

      if ($subsubcatlist) {
       $itemcount = count($subsubcatlist->find("li"));
       if ($itemcount > 0) {
          for ($i = 0; $i < $itemcount; $i++) {
            $item = $subsubcatlist->find("a", $i);
            if ($item) {
              $href = $item->href;
              $name = trim($item->innertext);
              $categories[$i]['name'] = $name;
              $categories[$i]['href'] = $href;
            }
          }
       }
      }
    }
  }

  $page->clear();
  unset($page);

  return $categories;
}

/**
* Get all categories/subcats/subsubcats in hierarchal array for one top category
* You should call this function separately for each top category and take
* whatever action the calling function needs to take in order to prevent a
* php timeout.
*/
function store_interaction_api_etsy_getonecategoryhierarchy($topcategoryindex) {
  set_time_limit(0);

  $categories = array();

  $categories = store_interaction_api_etsy_gettopcategories();

  //get subcategories
  if (count($categories)>=$topcategoryindex) {
    $subcaturl = "http://www.etsy.com/" . $categories[$topcategoryindex]['href'];
    $categories[$topcategoryindex]['subcategories'] = store_interaction_api_etsy_getsubcategories($subcaturl);
  }

  //get subsubcategories
  if (count($categories[$topcategoryindex]['subcategories']) > 0) {
    for($i = 0; $i < count($categories[$topcategoryindex]['subcategories']); $i++) {
     if (!$categories[$topcategoryindex]['subcategories'][$i]['href']) {

       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "ERROR ERROR ERROR subcat $i <br>";
       echo "<pre>";
       print_r($categories[$topcategoryindex]);
       echo "</pre>";
       exit;
     }

      $subsubcaturl = "http://www.etsy.com/" . $categories[$topcategoryindex]['subcategories'][$i]['href'];

      $categories[$topcategoryindex]['subcategories'][$i]['subsubcategories'] =
        store_interaction_api_etsy_getsubsubcategories($subsubcaturl);
    }
  }

  $result = array();
  $result[$topcategoryindex] = $categories[$topcategoryindex];
  return $result;

}



?>