<?php
/**
* @file
* Non public API funtions for etsy API functions, used by including file.
* The term "Etsy" is a trademark of Etsy, Inc.
* This API is not endorsed or certified by Etsy, Inc.
* This API does not use the Etsy API.
*/

/**
* Internal function Get array of categories
*/
function store_interaction_internal_etsy_getcategories($url, $tag, $id) {
  $categories = array();

  $page = file_get_html($url);

  $list = $page->find($tag . "[id=" . $id. "]", 0);

  if ($list) {

   $list_of_li = $list->find("li");

   if (count($list_of_li) > 0) {
     for ($i = 0; $i < count($list_of_li); $i++) {

       $li_item = $list->find("li", $i)->find("a", 0);
       $name = trim($li_item->innertext);
       //$title = $li_item->title;
       $href = $li_item->href;
       $categories[$i]['name'] = $name;
       $categories[$i]['href'] = $href;
     }
    }
  }

  $page->clear();
  unset($page);

  return $categories;

}


?>