<?php

/**
* @file
* Store Interaction API reads and writes data from/to others web stores.
*/

require_once("store_interaction_api_etsy.php");

/**
* Determine if file looks like a valid product backup file
* @param xmlfile the filename or filepath of the xml file (formatted by Store Interaction API)
* @return true or false
*/
function store_interaction_api_validbackupfile($xmlfile) {
  $result = TRUE;

  $xml = simplexml_load_file($xmlfile);
  if (!$xml) {$result = FALSE;}

  if (!isset($xml->shop)) {$result = FALSE;}

  return $result;
}

/**
* Get number of product items in backup file
* @param xmlfile the filename or filepath of the xml file (formatted by Store Interaction API)
* @return number of items or FALSE on error
*/
function store_interaction_api_productcount($xmlfile) {
  $xml = simplexml_load_file($xmlfile);
  $result = $xml->shop->itemcount;
  return $result;
}

/**
* Get product details
* @param xmlfile the filename or filepath of the xml file (formatted by Store Interaction API)
* @param itemnumber the item to get starting with 1
* @return array of product details or FALSE if error
*/
function store_interaction_api_getitem($xmlfile, $itemnumber, $imagedir='') {
  $result = array();

  if ($imagedir == '') {
    $imagedir = pathinfo($xmlfile, PATHINFO_DIRNAME) . '/';
  }

  $imagedir = rtrim($imagedir, '/');
  $imagedir = rtrim($imagedir, '\\');
  $imagedir .= '/';

  $xml = simplexml_load_file($xmlfile);

  $itemname = "item" . $itemnumber;

  $xmlitem = $xml->$itemname;

  if (!isset($xmlitem)) {
    return FALSE;
  }

  $result['itemnumber'] = (string) $xmlitem->itemnumber;
  $result['title'] = (string) $xmlitem->title;
  if (isset($xmlitem->costvalue)) {
    $result['cost'] = (string) $xmlitem0>costvalue;
  }
  else {
    $result['cost'] = '';
  }
  $result['price'] = (string) $xmlitem->pricevalue;
  //$result['payment_type'] = 'One Time';
  //$result['duration'] = 'None';
  if (isset($xmlitem->qty)) {
    $result['inventory'] =(string)  $xmlitem->qty;
  }
  else {
    $result['inventory'] = (string) $xmlitem->inventory;
  }
  $result['weight'] = (string) $xmlitem->weight;
  $result['width'] = (string) $xmlitem->width;
  $result['depth'] = (string) $xmlitem->depth;
  $result['height'] = (string) $xmlitem->height;
  $desc = (string) $xmlitem->description;
  $desc = base64_decode($desc);
  $result['description'] = $desc;
  //$result['option_groups_id'] = $ret['option_groups_id'];
  //$result['shipping_type'] = $ret['shipping_type'];
  $shippingitem = $xmlitem->shipping1;
  $result['shipping_cost'] = (string) $shippingitem->costalonevalue;
  //$result['tax_type'] = $ret['tax_type'];
  //$result['tax_cost'] = $ret['tax_cost'];
  //$result['category'] = $ret['category'];
  $result['meta_title'] = (string) $xmlitem->meta_title;
  $result['meta_description'] = (string) $xmlitem->meta_description;
  $result['meta_keywords'] = (string) $xmlitem->meta_keywords;

  $result['numpics'] = (int) $xmlitem->numpics;
  if ($result['numpics'] > 0) {
    $picitem = $xmlitem->photo1;

    if (file_exists($imagedir . $picitem->filename)) {
      $result['firstpic'] = $imagedir . $picitem->filename;
    }
  }
  return $result;
}

/**
 * Convert array of categories/subcats/subsubcats to XML
*/
function store_interaction_api_xmlfromcategoriesarray($categories_array) {
  $xml = new SimpleXMLElement('<xml/>');

  $catcount = 0;
  foreach($categories_array as $topcatindex=>$topcatitems) {
    $catcount++;
    $topcatchild = $xml->addChild('category' . $topcatindex);

    $topcatchild->addChild('name', $topcatitems['name']);

    $subcatcount = 0;
    //add subcats
    foreach($topcatitems['subcategories'] as $subcatindex=>$subcatitems) {
      $subcatcount++;
      $subcatchild = $topcatchild->addChild('subcategory' . $subcatindex);

      $subcatchild->addChild('name', $subcatitems['name']);

      $subsubcatcount = 0;
      //add sub sub categories
      foreach($subcatitems['subsubcategories'] as $subsubindex=>$subsubitem) {
        $subsubcatcount++;
        $subsubchild = $subcatchild->addChild('subsubcategory' . $subsubindex);

        $subsubchild->addChild('name', $subsubitem['name']);
      }
      $subcatchild->addChild('subsubcatcount', $subsubcatcount);

    }
    $topcatchild->addChild('subcatcount', $subcatcount);
  }

  //NOTE: if you are only calling this function with one category at a time, the
  //following will only have a value of 1, and then if you are gonig to merge xml
  //files (one for each category) you'll need to take out the extra <categorycount>
  //values and make sure there is only one in your final xml file
  $xml->addChild('categorycount', $catcount);

  return $xml;
}



