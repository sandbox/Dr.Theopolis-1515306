<?php
/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*/

$results_per_page = $_SESSION['userstorefront'][$storeowner_uid]['results_per_page'];

$login = $_GET['login'];

if ($login) {
	$_SESSION['userstorefront'][$storeowner_uid]['login'] = $login;
} else {
	$login = $_SESSION['userstorefront'][$storeowner_uid]['login'];
}

if ($login == "admin") {
	include('admin_login.inc');
}

if ($login == "member") {
	include('members_login.inc');
}

if ($login == "logout") {
	$_SESSION['userstorefront'][$storeowner_uid] = array();
}

echo "<div class=\"postcontent\">\n";

if ($admin_id and !$logout) {

	if (!$page) {

		echo("Welcome to the store admin interface!<p>");

		$db_lookup_orders = db_query("SELECT id FROM {usf_orders} WHERE uid = $storeowner_uid");
		$db_orders = @db_fetch_array($db_lookup_orders);
		$db_orders_id = $db_orders['id'];

		if ($db_orders_id) {

			echo("<hr size='1' color='#000000'><p>");

			echo("<b>Latest Orders</b><p>");

			echo("<p><table border='0' cellpadding='5' cellspacing='0'>");
			echo("<tr>");
			echo("<td>" . "<b>ID</b></td>");
			echo("<td>" . "<b>Date</b></td>");
			echo("<td>" . "<b>Time</b></td>");
			echo("<td>" . "<b>Payment</b></td>");
			echo("<td>" . "<b>Total</b></td>");
			echo("<td>" . "<b>Actions</b></td>");
			echo("</tr>");

			$db_lookup_orders = db_query("SELECT id, date, time, member_id, payment_status, order_total FROM {usf_orders} WHERE uid = %d ORDER BY id DESC LIMIT 0, 5", $storeowner_uid);

			while ($db_orders = @db_fetch_array($db_lookup_orders)) {

				$db_orders_id = $db_orders['id'];
				$db_orders_date = $db_orders['date'];
				$db_orders_time = $db_orders['time'];
				$db_orders_payment_status = $db_orders['payment_status'];
				$db_orders_order_total = $db_orders['order_total'];

				echo("<tr><td>" . $db_orders_id . "</td>");
				echo("<td>" . $db_orders_date . "</td>");
				echo("<td>" . $db_orders_time . "</td>");
				echo("<td>" . $db_orders_payment_status . "</td>");
				echo("<td>" . "$" . number_format($db_orders_order_total,2) . "</td>");
				echo("<td>" . "[<a href='" . $baseurl . "&page=orders&request=view_order&order_id=$db_orders_id'>View</a>] [<a href='" . $baseurl . "&page=orders&request=edit_order&order_id=$db_orders_id'>Edit</a>] [<a href='" . $baseurl . "&page=orders&request=delete_order&order_id=$db_orders_id'>Delete</a>]</td>");
				echo("</tr>");
			}
			echo("</table>");
		}

		$db_lookup_service = db_query("SELECT id FROM {usf_service} WHERE uid = %d", $storeowner_uid);
		$db_service = @db_fetch_array($db_lookup_service);
		$db_service_id = $db_service['id'];

		if ($db_service_id) {

			echo("<hr size='1' color='#000000'><p>");

			echo("<b>Latest Service</b><p>");

			echo("<table border='0' cellpadding='5' cellspacing='0'>");
			echo("<tr>");
			echo("<td>" . "<b>Order ID</b></td>");
			echo("<td>" . "<b>Member Name</b></td>");
			echo("<td>" . "<b>Status</b></td>");
			echo("<td>" . "<b>Actions</b></td>");
			echo("</tr>");

			$db_lookup_service = db_query("SELECT id, member_id, order_id, status FROM {usf_service} WHERE uid = %d ORDER BY id DESC LIMIT 0, 5", $storeowner_uid);

			while ($db_service = @db_fetch_array($db_lookup_service)) {

				$db_service_id = $db_service['id'];
				$db_service_member_id = $db_service['member_id'];
				$db_service_order_id = $db_service['order_id'];
				$db_service_status = $db_service['status'];

				$db_lookup_members = db_query("SELECT first_name, last_name FROM {usf_members} WHERE id='%s'", $db_service_member_id);

				while ($db_members = @db_fetch_array($db_lookup_members)) {

					$db_members_first_name = $db_members['first_name'];
					$db_members_last_name = $db_members['last_name'];

						echo("<tr>");
						echo("<td>" . $db_service_order_id . "</td>");
						echo("<td>" . $db_members_first_name . " " . $db_members_last_name . "</td>");
						echo("<td>" . $db_service_status . "</td>");
						echo("<td>" . "[<a href='" . $baseurl . "&page=service&request=view_service&service_id=$db_service_id'>View</a>]&nbsp;[<a href='" . $baseurl . "&page=service&request=edit_service&service_id=$db_service_id'>Edit</a>]&nbsp;[<a href='" . $baseurl . "&page=service&request=delete_service&service_id=$db_service_id'>Delete</a>]");
						echo("</td>");
						echo("</tr>");
				}
			}

			echo("</table>");
		}

  } else {

    if ($page == "orders") {
      include "catalog.inc";
    }
    else
    if (in_array($page, array("product_options","products","product_categories"))) {
      include "products.inc";
    }
    else
    if ($page == "service") {
      include "service.inc";
    }
  }

  echo("<p>");

}


if ($members_id and !$logout) {

  if ($page == 'orders') {

    include "orders.inc";

  } else {

    //not sure which of these is admin
    $catalog_pages = array(
      "member_orders",
      "checkout_phase_two",
      "checkout_phase_three",
      "checkout_phase_four",
      "checkout_phase_five");

    if (in_array($page, $catalog_pages)) {
      include ("catalog.inc");
    }
    else
    if ($page == "member_service") {
      include ("service.inc");
    }
    else
    if ($page == "profile") {
      include ("member.inc");
    }
    else
    if ($page == "addresses") {
      include ("member.inc");
    }
  }

  echo("<p>");
}

if (!$admin_id and !$members_id and !$login and !$logout) {


  /* google analytics code
  $db_lookup_admin_settings = db_query("SELECT setting FROM {usf_admin_settings} WHERE uid = $storeowner_uid AND setting_type='google_analytics_id'");
  $db_admin_settings = @db_fetch_array($db_lookup_admin_settings);
  $db_admin_settings_setting = $db_admin_settings['setting'];

  echo ("\r\r<script type=\"text/javascript\">\r");
  echo ("var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");\r");
  echo ("document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));\r");
  echo ("</script>\r");
  echo ("<script type=\"text/javascript\">\r");
  echo ("try{\r");
  echo ("var pageTracker = _gat._getTracker(\"$db_admin_settings_setting\");\r");
  echo ("pageTracker._trackPageview();\r");
  echo ("} catch(err) {}\r");
  echo ("</script>\r\r");
  */

  if (!$page) {

    echo "Welcome to " . user_storefront_get_storesetting($storeowner_uid, 'store_title');
    echo ("<p>");


    $db_lookup_products = db_query("SELECT id FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1'", $storeowner_uid);
    $db_products = @db_fetch_array($db_lookup_products);
    $db_products_id = $db_products['id'];

    if ($db_products_id) {

      echo ("<hr size='1' color='#000000'><p>");

      echo("<center><b>Shop - Latest Products</b></center><p>");

      echo("<table border='0' cellpadding='5' cellspacing='0' width='95%'>");

      $db_lookup_products = db_query("SELECT id FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 0, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];

        $db_lookup_products_images = db_query("SELECT small_image_name FROM {usf_products_images} WHERE products_id='%s' and main_image='yes'", $db_products_id);
        $db_products_images = @db_fetch_array($db_lookup_products_images);
        $db_products_images_small_image_name = $db_products_images['small_image_name'];

        echo("<td width='25%'><center>");
        if ($db_products_images_small_image_name) {

          echo("<a href='" . $baseurl . "&page=view_detail&request=$db_products_id'><img border='0' src='" . $image_view_dir . "shop_images/" . $db_products_images_small_image_name . "'></a>");
        }
        echo("</center></td>");
      }
      echo("</tr>\n");

      $db_lookup_products = db_query("SELECT id, code, name, price, payment_type, sub_time, sub_unit FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 0, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];
        $db_products_code = $db_products['code'];
        $db_products_name = $db_products['name'];
        $db_products_price = $db_products['price'];
        $db_products_payment_type = $db_products['payment_type'];
        $db_products_sub_time = $db_products['sub_time'];
        $db_products_sub_unit = $db_products['sub_unit'];

        if ($db_products_sub_time == "1") {
          $db_products_sub_unit = str_replace("D", "Day", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("W", "Week", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("M", "Month", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("Y", "Year", $db_products_sub_unit);
        } else {
          $db_products_sub_unit = str_replace("D", "Days", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("W", "Weeks", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("M", "Months", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("Y", "Years", $db_products_sub_unit);
        }

        echo("<td width='25%'><center>");
        if ($db_products_code) {
          echo("Model: $db_products_code<br>");
        }
        if ($db_products_name) {
          echo("Name: $db_products_name<br>");
        }
        if ($db_products_payment_type == "One Time") {
          echo("Price: $" . number_format($db_products_price,2));
        } else {
          echo("Price: $" . number_format($db_products_price,2) . " / $db_products_sub_time $db_products_sub_unit");
        }
        echo("</center></td>");
      }
      echo("</tr>");

      $db_lookup_products = db_query("SELECT id FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 0, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];

        echo("<td width='25%'><center>");
        echo("[<a href='" . $baseurl . "&page=view_detail&request=$db_products_id'>Details</a>]");
        echo("</center></td>");
      }
      echo("</tr>");

      $db_lookup_products = db_query("SELECT id FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 4, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];

        $db_lookup_products_images = db_query("SELECT small_image_name FROM {usf_products_images} WHERE uid = %d AND products_id='%s' and main_image='yes'", $storeowner_uid, $db_products_id);
        $db_products_images = @db_fetch_array($db_lookup_products_images);
        $db_products_images_small_image_name = $db_products_images['small_image_name'];

        echo("<td width='25%'><center>");
        if ($db_products_images_small_image_name) {
          echo("<a href='" . $baseurl . "&page=view_detail&request=$db_products_id'><img border='0' src='" . $image_view_dir . "shop_images/" . $db_products_images_small_image_name . "'></a>");
        }
        echo("</center></td>");
      }
      echo("</tr>");

      $db_lookup_products = db_query("SELECT id, code, name, price, payment_type, sub_time, sub_unit FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 4, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];
        $db_products_code = $db_products['code'];
        $db_products_name = $db_products['name'];
        $db_products_price = $db_products['price'];
        $db_products_payment_type = $db_products['payment_type'];
        $db_products_sub_time = $db_products['sub_time'];
        $db_products_sub_unit = $db_products['sub_unit'];

        if ($db_products_sub_time == "1") {
          $db_products_sub_unit = str_replace("D", "Day", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("W", "Week", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("M", "Month", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("Y", "Year", $db_products_sub_unit);
        } else {
          $db_products_sub_unit = str_replace("D", "Days", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("W", "Weeks", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("M", "Months", $db_products_sub_unit);
          $db_products_sub_unit = str_replace("Y", "Years", $db_products_sub_unit);
        }

        echo("<td width='25%'><center>");
        if ($db_products_code) {
          echo("Model: $db_products_code<br>");
        }
        if ($db_products_name) {
          echo("Name: $db_products_name<br>");
        }
        if ($db_products_payment_type == "One Time") {
          echo("Price: $" . number_format($db_products_price,2));
        } else {
          echo("Price: $" . number_format($db_products_price,2) . " / $db_products_sub_time $db_products_sub_unit");
        }
        echo("</center></td>");
      }
      echo("</tr>");

      $db_lookup_products = db_query("SELECT id FROM {usf_products} WHERE uid = %d AND mode='On' and inventory >= '1' ORDER BY id DESC LIMIT 4, 4", $storeowner_uid);

      echo("<tr>");

      while ($db_products = @db_fetch_array($db_lookup_products)) {

        $db_products_id = $db_products['id'];

        echo("<td width='25%'><center>");
        echo("[<a href='" . $baseurl . "&page=view_detail&request=$db_products_id'>Details</a>]");
        echo("</center></td>");
      }
      echo("</tr>");

      echo("</table>");
    }
    else
    {
      echo "There are no products in this store.<br><br>";
    }


  } else {

    $include = $_GET['include'];

    //a lot of these are not needed
    $catalog_pages = array(
      "catalog",
      "view_category",
      "view_detail",
      "checkout",
      "view_cart",
      "member_orders",
      "checkout_phase_two",
      "checkout_phase_three",
      "checkout_phase_four",
      "checkout_phase_five");

    if (in_array($page, $catalog_pages)) {
      include ("catalog.inc");
    }
    else
    if ($page == "member_service") {
      include ("service.inc");
    }
    else
    if ($page == "profile") {
      include ("member.inc");
    }
    else
    if ($page == "addresses") {
      include ("member.inc");
    }
    else
    if ($page == "policies") {
      include ("policies.inc");
    }
    else
    if ($page == "contact") {
      include ("contact.inc");
    }
    else
    if ($page == "about") {
      include ("about.inc");
    }
    else
    if ($page == "policies") {
      include ("policies.inc");
    }

    //the following can be removed once I have found all the places affected
    $include = $_GET['include'];
    if ($include) {
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";
      echo "WARNING WARNING WARNING this shouldn't be happening themes_body.inc including $include from GET parameter<br><br>";

      include($include);
    }


  }

  echo "</div>\n";


}

?>