<?php
/**
* @file
* Drupal version of the file by the same name in the Free Simple Shop script.
*
* This file sets up variables with contents that indicates to all the other code
* in the module that the current user is the owner of the storefront.
*/

$admin_id = $_SESSION['userstorefront'][$storeowner_uid]['admin_id'];
$admin_first_name = $_SESSION['userstorefront'][$storeowner_uid]['admin_first_name'];
$admin_last_name = $_SESSION['userstorefront'][$storeowner_uid]['admin_last_name'];
$admin_email_address = $_SESSION['userstorefront'][$storeowner_uid]['admin_email_address'];
$admin_password = $_SESSION['userstorefront'][$storeowner_uid]['admin_password'];
$admin_rights = $_SESSION['userstorefront'][$storeowner_uid]['admin_rights'];

$page = $_GET['page'];
$request = $_GET['request'];

$results_per_page = $_SESSION['userstorefront'][$storeowner_uid]['results_per_page'];


?>